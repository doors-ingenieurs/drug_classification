Drug Classification Notebook
===========================

This repository contains a Jupyter Notebook that performs exploratory data analysis (EDA) and trains machine learning models to classify drugs based on patient characteristics.

Dataset
-------

The dataset used in this notebook is named `drug200.csv`. It contains information about various drugs and the patients who take them, including age, sex, blood pressure, cholesterol level, and sodium to potassium ratio.

Notebook Content
----------------

The Jupyter Notebook included in this repository provides the following functionality:

- Data loading and initial exploration
- Data visualization using popular Python libraries (NumPy, Pandas, Matplotlib, Seaborn)
- Data preprocessing and feature engineering
- Training and evaluation of multiple machine learning models:
  - Logistic Regression
  - K-Nearest Neighbors
  - Support Vector Machine
  - Categorical Naive Bayes
  - Gaussian Naive Bayes
  - Decision Tree
  - Random Forest
